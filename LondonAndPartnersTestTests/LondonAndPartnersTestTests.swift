//
//  LondonAndPartnersTestTests.swift
//  LondonAndPartnersTestTests
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import XCTest
@testable import LondonAndPartnersTest

class LondonAndPartnersTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchMovies()
    {
        let expect = expectation(description: "20 movies expected")
        
        MoviesClient.fetchMovieItems { movieItems, error in
            if let recievedMovieItems = movieItems
            {
                let count = recievedMovieItems.count
                XCTAssert(recievedMovieItems.count != 0, "No results")
                XCTAssert(recievedMovieItems.count == 20, "\(count) results returned, 20 expected")
            }
            
            XCTAssertNil(error, "Unexpected error occured: \(error?.localizedDescription ?? ""))")
            
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: { (error) in
            XCTAssertNil(error, "Timeout: \(error?.localizedDescription ?? ""))")
        })
    }
    
}
