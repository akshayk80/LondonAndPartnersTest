//
//  LondonAndPartnersTestUITests.swift
//  LondonAndPartnersTestUITests
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import XCTest

class LondonAndPartnersTestUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTableViewWithData()
    {
        let app = XCUIApplication()
        let query = app.descendants(matching: .any).matching(identifier: "LPMoviesTableViewId")
        let tableView : XCUIElement = query.element(boundBy: 0)
        
        let count = tableView.cells.count
        XCTAssert(count > 0, "No results")
        XCTAssert(count == 20, "\(count) results returned, 20 expected")
    }
    
}
