//
//  MovieImageView.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import Foundation
import UIKit

/** Cached images once downloaded. */
let imageCache = NSCache<NSString, AnyObject>()

class MovieImageView : UIImageView
{
    /** url string for each image to store in cache. */
    var imageUrlString : String?
    
    /**
     Fetch the image from a given url and update image of the imageView.
     @param urlString to fetch image from.
     */
    public func imageFromURL(urlString: String)
    {
        imageUrlString = urlString
        let url = NSURL(string: urlString)! as URL
        
        self.image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) as? UIImage
        {
            self.image = imageFromCache
        }
        else
        {
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) -> Void in
                
                if error != nil {
                    print(error?.localizedDescription ?? "Error")
                    return
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    let imageToCache = UIImage(data: data!)
                    
                    if self.imageUrlString == urlString
                    {
                        self.image = imageToCache
                    }
                    imageCache.setObject(imageToCache!, forKey: urlString as NSString)
                })
                
            }).resume()
        }
        
    }
}

