//
//  MovieCell.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import Foundation
import UIKit

/**
 Custom Table View Cell to display data.
 */
class MovieCell: UITableViewCell
{
    /** Label to display title of the movie. */
    @IBOutlet weak var titleLabel : UILabel?
    
    /** Label to display overview of the movie. */
    @IBOutlet weak var overviewTextView : UITextView?
    
    /** Label to display average rating of the movie. */
    @IBOutlet weak var averageLabel : UILabel?
    
    /** ImageView for movie image. */
    @IBOutlet weak var movieImageView : MovieImageView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
        
        self.overviewTextView?.isUserInteractionEnabled = false
    }
}
