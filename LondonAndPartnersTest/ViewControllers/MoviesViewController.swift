//
//  MoviesViewController.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import UIKit
import CoreData

let kRowHeight : CGFloat = 180

class MoviesViewController: UITableViewController
{
    /** View Model for each movie item. */
    var movieItemViewModel = MovieItemViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = NSLocalizedString("Movies", comment: "")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.cellLayoutMarginsFollowReadableWidth = false
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.rowHeight = kRowHeight
        self.tableView.accessibilityIdentifier = "LPMoviesTableViewId"
        
        self.movieItemViewModel.movieItemViewModelDelegate = self
        
        do
        {
            try self.movieItemViewModel.fetchedhResultController.performFetch()
        }
        catch let error
        {
            print("Error: \(error)")
        }
        
        movieItemViewModel.fetchMovieItems
        {
            DispatchQueue.main.async
            {
                self.tableView.reloadData()
                
            }
        }
    }
    
    // MARK: UITableViewDelegate Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return movieItemViewModel.numberOfItemsInSection(section: section)
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
        
        configureCell(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    // MARK: Methods
    
    /**
     Configure cell elements (title, overview, average, icon image).
     @param cell to configure.
     @param indexPath for the cell to configure.
     */
    func configureCell(cell: MovieCell, indexPath: IndexPath)
    {
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.titleLabel?.text = movieItemViewModel.titleForItemAtIndexPath(indexPath: indexPath)
        cell.overviewTextView?.text = movieItemViewModel.overviewForItemAtIndexPath(indexPath: indexPath)
        cell.averageLabel?.text = movieItemViewModel.averageForItemAtIndexPath(indexPath: indexPath)
        
        let imageUrl = movieItemViewModel.iconUrlForItemAtIndexPath(indexPath: indexPath)
        cell.movieImageView?.imageFromURL(urlString: imageUrl)
        
        cell.movieImageView?.contentMode = UIViewContentMode.scaleAspectFit
        
    }
}

/**
 MovieItemViewModelDelegate Methods to update rows from cache
 */
extension MoviesViewController: MovieItemViewModelDelegate {
    
    func insertRows(at indexPath: [IndexPath], with animation: UITableViewRowAnimation)
    {
        self.tableView.insertRows(at: indexPath, with: animation)
    }
    
    func deleteRows(at indexPath: [IndexPath], with animation: UITableViewRowAnimation)
    {
        self.tableView.deleteRows(at: indexPath, with: animation)
    }
    
    func tableViewBeginUpdates()
    {
        self.tableView.endUpdates()
    }
    
    func tableViewEndUpdates()
    {
        tableView.beginUpdates()
    }
    
    
}

