//
//  CoreDataMovies.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataMovies: NSObject {
    
    static let sharedInstance = CoreDataMovies()
    private override init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreDataMovies")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unknown error: \(error)")
            }
        })
        return container
    }()
    
    func saveContext ()
    {
        let context = persistentContainer.viewContext
        if context.hasChanges
        {
            do
            {
                try context.save()
            }
            catch
            {
                fatalError("Unknown error: \(error)")
            }
        }
    }
    
    /** Print library directory path in console to debug saved data in sqlite database. */
    func applicationDocumentsDirectory()
    {
        if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
            print(url.absoluteString)
        }
    }
}
