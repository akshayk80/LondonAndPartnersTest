//
//  UrlItem.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

/**
 Model to represent a Movie Item.
 */

import CoreData

struct MovieItem : Decodable
{
    /** title of the movie item. */
    let title : String
    
    /** overview  of the movie item. */
    let overview : String
    
    /** average of the movie item. */
    let vote_average : Double
    
    /** image url of the movie item. */
    let poster_path : String
}

/** Managed movie item object to save in database. */
class MovieItemManaged : NSManagedObject
{
    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieItemManaged> {
        return NSFetchRequest<MovieItemManaged>(entityName: "MovieItemManaged");
    }
    
    /** title of the movie item. */
    @NSManaged public var title : String

    /** overview  of the movie item. */
    @NSManaged public var overview : String

    /** average of the movie item. */
    @NSManaged public var vote_average : Double

    /** image url of the movie item. */
    @NSManaged public var poster_path : String
}

struct Root : Decodable
{
    /** page number of the list of movies. */
    let page : Int
    
    /** all movies list */
    let results : [MovieItem]
}

