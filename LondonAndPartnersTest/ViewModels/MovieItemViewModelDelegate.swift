//
//  MovieItemViewModelDelegate.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/14.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import Foundation
import UIKit

protocol MovieItemViewModelDelegate
{
    func tableViewBeginUpdates()
    func tableViewEndUpdates()
    
    func insertRows(at indexPath: [IndexPath], with animation: UITableViewRowAnimation)
    func deleteRows(at indexPath: [IndexPath], with animation: UITableViewRowAnimation)
}
