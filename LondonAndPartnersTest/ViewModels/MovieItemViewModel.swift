//
//  MovieItemViewModel.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import UIKit
import CoreData

let kBaseUrlForImage : String = "https://image.tmdb.org/t/p/w500"

/**
 ViewModel for Movie Item created from JSON data.
 */
class MovieItemViewModel : NSObject, NSFetchedResultsControllerDelegate
{
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: MovieItemManaged.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "vote_average", ascending: false)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataMovies.sharedInstance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    /** MovieItemViewModelDelegate to refresh table view rows. */
    var movieItemViewModelDelegate : MovieItemViewModelDelegate?
    
    /** Custom object to hold information for each url item fetched. */
    var movieItems = [MovieItem]()
    
    /**
     Fetch url items to initialise the ViewModel.
     @param completion to be called once task for fetching data is completed.
     */
    func fetchMovieItems(completion: @escaping () -> ())
    {
        MoviesClient.fetchMovieItems { movieItems, error  in
            if let recievedMovieItems = movieItems
            {
                self.movieItems = recievedMovieItems
                
                // clear core data
                self.clearData()
                
                // save movie items to core data
                self.saveInCoreDataWith(movieItems: self.movieItems)
            }
            
            completion()
        }
    }
    
    /**
     Provides the number of items in a table view section. As there is only one section, this equals to overall number of items which can be displayed.
     @param section for which number of items to return.
     @returns Number of items.
     */
    func numberOfItemsInSection(section: Int) -> Int
    {
        if let count = fetchedhResultController.sections?[section].numberOfObjects {
            return count
        }
        return 0
    }
    
    /**
     Provides the title for passed in indexPath.
     @param indexPath for the item.
     @returns title of the item.
     */
    func titleForItemAtIndexPath(indexPath: IndexPath) -> String
    {
        if let movie = fetchedhResultController.object(at: indexPath) as? MovieItemManaged
        {
            return movie.title
        }
        return ""
    }
    
    /**
     Provides the url of the icon for passed in indexPath.
     @param indexPath for the item.
     @returns icon url of the item.
     */
    func iconUrlForItemAtIndexPath(indexPath: IndexPath) -> String
    {
        if let movie = fetchedhResultController.object(at: indexPath) as? MovieItemManaged
        {
            return kBaseUrlForImage + movie.poster_path
        }
        return ""
    }
    
    /**
     Provides the overview of item for passed in indexPath.
     @param indexPath for the item.
     @returns overview of the item.
     */
    func overviewForItemAtIndexPath(indexPath: IndexPath) -> String
    {
        if let movie = fetchedhResultController.object(at: indexPath) as? MovieItemManaged
        {
            return movie.overview
        }
        return ""
    }
    
    /**
     Provides the average value of the item for passed in indexPath.
     @param indexPath for the item.
     @returns average text of the item.
     */
    func averageForItemAtIndexPath(indexPath: IndexPath) -> String
    {
        if let movie = fetchedhResultController.object(at: indexPath) as? MovieItemManaged
        {
            return "Average: \(movie.vote_average)"
        }
        return ""
    }
    
    /**
     Creates a movie entity in database.
     @param MovieItem object.
     @returns Optional ManagedMovieItem object.
     */
    private func createMovieEntityFrom(movieItem: MovieItem) -> NSManagedObject? {
        
        let context = CoreDataMovies.sharedInstance.persistentContainer.viewContext
        if let movieEntity = NSEntityDescription.insertNewObject(forEntityName: "MovieItemManaged", into: context) as? MovieItemManaged {
            movieEntity.title = movieItem.title
            movieEntity.overview = movieItem.overview
            movieEntity.poster_path = movieItem.poster_path
            movieEntity.vote_average = movieItem.vote_average
            return movieEntity
        }
        return nil
    }
    
    /**
     Save each movie item in database.
     @param List of MovieItem objects.
     */
    private func saveInCoreDataWith(movieItems: [MovieItem]) {
        do
        {
            for item : MovieItem in movieItems
            {
                _ = self.createMovieEntityFrom(movieItem: item)
            }
            
            do
            {
                try CoreDataMovies.sharedInstance.persistentContainer.viewContext.save()
            }
            catch let error
            {
                print(error)
            }
        }
    }
    
    /**
     Delete all movie items in database.
     */
    private func clearData()
    {
        do
        {
            let context = CoreDataMovies.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: MovieItemManaged.self))
            do
            {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map { $0.map { context.delete($0) } }
                CoreDataMovies.sharedInstance.saveContext()
            }
            catch let error
            {
                print("Error: \(error)")
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //MARK: NSFetchedResultsControllerDelegate Methods --------------------------------------------------
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            movieItemViewModelDelegate?.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            movieItemViewModelDelegate?.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        movieItemViewModelDelegate?.tableViewBeginUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        movieItemViewModelDelegate?.tableViewEndUpdates()
    }
}

