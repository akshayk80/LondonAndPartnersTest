//
//  MoviesClient.swift
//  LondonAndPartnersTest
//
//  Created by AkshayKapoor InternalTester01 on 2018/03/13.
//  Copyright © 2018 LondonAndPartners. All rights reserved.
//

import Foundation
import CoreData

/**
 Movies Client to fetch JSON results.
 */
class MoviesClient : NSObject
{
    /**
     Fetch Url items.
     @param completion to call with items fethced
     */
    class func fetchMovieItems(completion: @escaping ([MovieItem]?, _ error: Error?) -> ())
    {
        let urlString = "https://api.themoviedb.org/3/movie/top_rated?api_key=449d682523802e0ca4f8b06d8dcf629c&language=en-US&page=1"
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            
            if error != nil
            {
                completion(nil, error)
                return
            }
            else
            {
                do
                {
                    let items = try JSONDecoder().decode(Root.self, from: data)
                    completion(items.results, nil)
                    
                    return
                }
                catch let jsonErr
                {
                    print("Error serializing json:", jsonErr)
                }
            }
        }
        task.resume()
    }
}
